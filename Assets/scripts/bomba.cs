﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
using TMPro;
public class bomba : MonoBehaviour
{
    GameObject gameController;

    public int bombCounter;  //0 oldugunda patlayacak
    public GameObject text; //geri sayimin text te gosterilmesi icin

    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");

        bombCounter = Random.Range(5, 8);
       

        text.GetComponent<TextMeshPro>().text = bombCounter.ToString();

    }

    public void action()
    {
        bombCounter--;

        if (bombCounter==0)
        {
            Time.timeScale = 0;
         gameController.GetComponent<GameController>().gameOverPanel.SetActive(true);
        }
        
           
            text.GetComponent<TextMeshPro>().text = bombCounter.ToString();
 
      
    }

   
   
}
