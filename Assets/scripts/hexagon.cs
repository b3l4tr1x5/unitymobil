﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class hexagon : MonoBehaviour
{

    public GameObject[] collDetect;  //tespit eden

    public GameObject[] hexagons; //etrafindaki tespit edilen hexagonlar

    public int colorID;  //bu hexagonun color id si

    GameObject gameController;  //main script burada

    public int rowID;
    LayerMask m;
    public bool ready=false; //havada gelirken patlamasin vs diye..

    public bool kontrolEdildimi=false;      //game over logic icin.. donunce patlayabilirmi kontrolu edildimi
    static bool destroy;
    void Start()
    {

        gameController = GameObject.FindGameObjectWithTag("GameController");

        gameObject.layer = 8;
        m = ~m;

          InvokeRepeating("setPoint", 6, 0.5f); //her 1 saniyede etrafindaki nesneleri kontrol ediyor patlama olayi icin..yüksek performans icin boyle kullanildi
        destroy = false;
       // gameObject.tag = "";
    }

 


   public  void OnDestroy()  //nesene yok oldugunda calisacak fonksion .. bu row da hexa azalmis demek spawn olmasini sagliyor..
    {
      StartCoroutine(  patlatmaBeklett());
        destroy = true;
        setPoint();
        gameController.GetComponent<GameController>().spawners[rowID].GetComponent<spawner>().buradaKacTaneVar--;  //editorde error verebilir, sorun yok
      
    }
    IEnumerator patlatmaBeklett( )
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.010f));
    }


    void setPoint() //hexanin yeri degistiginde rowID yi set etmek icin yapildi.. hexa patladiginda bu rowID ye göre yenisi spawnaniyor
    {
        ready = true; //spawnlandiktan x saniye sonra hazir hale geliyor
        gameObject.tag = "hexagon";


        RaycastHit hit;
        Ray ray = new Ray(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), Vector3.up);
     

        if (Physics.Raycast(ray, out hit, 1000000,LayerMask.GetMask("spawner")))
                {
 
                    if (hit.collider.gameObject.tag == "spawner")
                    {            
                        rowID = hit.collider.GetComponent<spawner>().spawnerID;                     
                    }
           
            //Debug.DrawRay(ray.origin, ray.direction, Color.green, 10000f); 
        }

 
        if(!destroy)
          gameController.GetComponent<GameController>().patlatma(gameObject);

    }


    public void triggerDetect(Collider childTrigger,int bolge) //trigger a giren hexagon , o tarafa bakan trigger ile ayni siraya koyuluyor
    {
        if(childTrigger.gameObject.tag=="hexagon" )
        {         
            hexagons[bolge] = childTrigger.gameObject;            
        }             
    }
}
