﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public int spawnerID;    //bunun ID si
    public GameObject spawnObje;  //ne spawnlanacak

    GameController gameController;  //main script

    public Material[] materials;  //sawpn da kullanilacak materyaller

    public int spawnCounter;    //üst üste spawnlanmamasi icin

    public int buradaKacTaneVar;

    int bombPuan;
    void Start()
    {
 
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();  //gamecontroller dan gerekli bilgiler aliniyor

        spawnObje = gameController.hexagon;

        bombPuan = gameController.GetComponent<GameController>().bombPuan;
 
        StartCoroutine(baslangicSpawn()); //spawnlanirken güzel gözükmesi icin

        InvokeRepeating("spawnController",9,1);  //üst üste spawnlamalari önlemek icin hizli çözüm.. saniyede 1 kere spawnlanacak sayiya bakiyor
 
    }



  public  void spawn(bool bomba=false)
    {
        buradaKacTaneVar++;
        int matID = Random.Range(0,gameController.GetComponent<GameController>().colourSayisi);
        spawnObje.GetComponent<Renderer>().material = materials[matID];
        spawnObje.GetComponent<hexagon>().colorID = matID;
        spawnObje.GetComponent<hexagon>().rowID = spawnerID;

        if (!bomba)
            gameController.GetComponent<GameController>().allHexagons.Add(Instantiate(spawnObje, transform.position, transform.rotation));  //hexagon
        else
            gameController.GetComponent<GameController>().bombalar.Add(Instantiate(gameController.GetComponent<GameController>().bomba, transform.position, gameController.GetComponent<GameController>().bomba.transform.rotation)); //bomba

    }

    void spawnController()   //bu rowda eksik hexagon varsa spawnliyor .. bomba puani belirlenen sayidan fazlaysa bomba spawnliyor
    {
        if (buradaKacTaneVar < gameController.stun)
        {
             
            int bombCounter = gameController.GetComponent<GameController>().bombCounter;
 
            
            if (gameController.GetComponent<GameController>().puan> bombCounter * bombPuan && bombCounter * bombPuan< bombCounter*bombPuan +1)
            {
                
                   gameController.GetComponent<GameController>().bombCounter++;
                spawn(true);
            }
            else
                spawn();
        }
    }

    IEnumerator baslangicSpawn()
    {
       // Random.InitState(spawnerID);
        yield return new WaitForSeconds(Random.Range(0.4f,0.9f));
        for (int i = 0; i < gameController.stun; i++)
        {
            spawn();
            yield return new WaitForSeconds(0.5f);
        }    
    }
 
 
}
