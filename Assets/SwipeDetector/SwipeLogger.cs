﻿using UnityEngine;

public class SwipeLogger : MonoBehaviour
{
    public GameObject gameController;
    private void Awake()
    {
        SwipeDetector.OnSwipe += SwipeDetector_OnSwipe;
    }

    private void SwipeDetector_OnSwipe(SwipeData data)
    {
        Debug.Log("Swipe in Direction: " + data.Direction);

        if (data.Direction == SwipeDirection.Up)
            gameController.GetComponent<GameController>().saatYonu = 2;
           
        if (data.Direction == SwipeDirection.Down)
            gameController.GetComponent<GameController>().saatYonu =1;

        gameController.GetComponent<GameController>().hexaDonzer();
    }
}